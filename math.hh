#ifndef MATH_HH
#define MATH_HH

#include "defines.hh"
#include <cstdlib>
#include <cmath>
#include <random>

inline f32 Abs(const f32 _a)
{
    return _a < 0 ? -_a : _a;
}

inline f32 Lerp(const f32 _a, const f32 _t, const f32 _b)
{
    return ((1.0f - _t) * _a) + (_t * _b);
}

inline f32 Square(const f32 _a)
{
    return _a * _a;
}

inline f32 SquareRoot(const f32 _a)
{
    return (f32)std::sqrt(_a);
}

inline f32 Pow(const f32 _a, const f32 _b)
{
    return (f32)std::pow(_a, _b);
}

inline u32 RoundF32ToU32(f32 _f)
{
    return (u32)(_f + 0.5f);
}

struct RandomSeries
{
    u32 state;
};

inline u32 XORShift32(RandomSeries *_series)
{
    u32 x = _series->state;
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    _series->state = x;
    return x;
}

inline f32 RandomUnilateral(RandomSeries *_series)
{
    return (f32)XORShift32(_series) / (f32)U32_MAX;
}

inline f32 RandomBilateral(RandomSeries *_series)
{
    return -1.0f + 2.0f * RandomUnilateral(_series);
}

union v3
{
    struct
    {
        f32 x, y, z;
    };
    struct
    {
        f32 r, g, b;
    };
    f32 e[3];

    v3(f32 _x = 0.f, f32 _y = 0.f, f32 _z = 0.f) : x(_x), y(_y), z(_z) {};
};

inline v3 operator*(const v3 &_a, const f32 _b)
{
    v3 result;
    result.x = _a.x * _b;
    result.y = _a.y * _b;
    result.z = _a.z * _b;
    return result;
}

inline v3 operator*(const f32 _b, const v3 &_a)
{
    return _a * _b;
}

inline v3 operator/(const v3 &_a, const f32 _b)
{
    v3 result;
    result.x = _a.x / _b;
    result.y = _a.y / _b;
    result.z = _a.z / _b;
    return result;
}

inline v3 operator+(const v3 &_a, const v3 &_b)
{
    v3 result;
    result.x = _a.x + _b.x;
    result.y = _a.y + _b.y;
    result.z = _a.z + _b.z;
    return result;
}

inline v3 operator-(const v3 &_a, const v3 &_b)
{
    v3 result;
    result.x = _a.x - _b.x;
    result.y = _a.y - _b.y;
    result.z = _a.z - _b.z;
    return result;
}

inline v3 operator-(const v3 &_a)
{
    v3 result;
    result.x = -_a.x;
    result.y = -_a.y;
    result.z = -_a.z;
    return result;
}

inline v3 &operator*=(v3 &_a, const f32 _b)
{
    _a.x *= _b;
    _a.y *= _b;
    _a.z *= _b;
    return _a;
}

inline v3 &operator/=(v3 &_a, const f32 _b)
{
    _a.x /= _b;
    _a.y /= _b;
    _a.z /= _b;
    return _a;
}

inline v3 &operator+=(v3 &_a, const v3 &_b)
{
    _a.x += _b.x;
    _a.y += _b.y;
    _a.z += _b.z;
    return _a;
}

inline v3 &operator-=(v3 &_a, const v3 &_b)
{
    _a.x -= _b.x;
    _a.y -= _b.y;
    _a.z -= _b.z;
    return _a;
}

inline v3 Cross(const v3 &_a, const v3 &_b)
{
    v3 result;

    result.x = _a.y * _b.z - _a.z * _b.y;
    result.y = _a.z * _b.x - _a.x * _b.z;
    result.z = _a.x * _b.y - _a.y * _b.x;

    return result;
}

inline v3 Hadamard(const v3 &_a, const v3 &_b)
{
    v3 result;
    result.x = _a.x * _b.x;
    result.y = _a.y * _b.y;
    result.z = _a.z * _b.z;
    return result;
}

inline f32 Inner(const v3 &_a, const v3 &_b)
{
    return _a.x * _b.x + _a.y * _b.y + _a.z * _b.z;
}

inline v3 Lerp(const v3 &_a, const f32 _t, const v3 &_b)
{
    v3 result;
    result.x = Lerp(_a.x, _t, _b.x);
    result.y = Lerp(_a.y, _t, _b.y);
    result.z = Lerp(_a.z, _t, _b.z);
    return result;
}

inline f32 LengthSq(const v3 &_a)
{
    return Inner(_a, _a);
}

inline f32 Length(const v3 &_a)
{
    return SquareRoot(LengthSq(_a));
}

inline v3 Normal(const v3 &_a)
{
    return _a * (1.0f / Length(_a));
}

inline v3 NOZ(const v3 &_a)
{
    v3 result = {};

    f32 lenSq = LengthSq(_a);
    if (lenSq > Square(0.0001f))
    {
        result = _a * (1.0f / SquareRoot(lenSq));
    }

    return result;
}

union v4
{
    struct
    {
        f32 x, y, z, w;
    };
    struct
    {
        f32 r, g, b, a;
    };
    f32 e[4];

    v4(f32 _x = 0.f, f32 _y = 0.f, f32 _z = 0.f, f32 _w = 0.f) : x(_x), y(_y), z(_z), w(_w) {};
    v4(v3 _xyz, f32 _w) : x(_xyz.x), y(_xyz.y), z(_xyz.z), w(_w){};
};

inline v4 operator*(const v4 &_a, const f32 _b)
{
    v4 result;
    result.x = _a.x * _b;
    result.y = _a.y * _b;
    result.z = _a.z * _b;
    result.w = _a.w * _b;
    return result;
}

inline v4 operator/(const v4 &_a, const f32 _b)
{
    v4 result;
    result.x = _a.x / _b;
    result.y = _a.y / _b;
    result.z = _a.z / _b;
    result.w = _a.w / _b;
    return result;
}

inline v4 &operator+=(v4 &_a, const v4 &_b)
{
    _a.x += _b.x;
    _a.y += _b.y;
    _a.z += _b.z;
    _a.w += _b.w;
    return _a;
}

inline f32 Inner(const v4 &_a, const v4 &_b)
{
    return _a.x * _b.x + _a.y * _b.y + _a.z * _b.z + _a.w * _b.w;
}

inline v4 ABGRUnpack4x8(u32 _packed)
{
    return v4((f32)((_packed >> 24) & 0xFF),
              (f32)((_packed >> 16) & 0xFF),
              (f32)((_packed >> 8) & 0xFF),
              (f32)((_packed >> 0) & 0xFF));
}

inline v4 BGRAUnpack4x8(u32 _packed)
{
    return v4((f32)((_packed >> 16) & 0xFF),
              (f32)((_packed >> 8) & 0xFF),
              (f32)((_packed >> 0) & 0xFF),
              (f32)((_packed >> 24) & 0xFF));
}

inline u32 BGRAPack4x8(const v4 &_unpacked)
{
    return ((RoundF32ToU32(_unpacked.a) << 24) |
            (RoundF32ToU32(_unpacked.r) << 16) |
            (RoundF32ToU32(_unpacked.g) << 8) |
            (RoundF32ToU32(_unpacked.b) << 0));
}

inline f32 ExactLinearToSRGB(f32 _linear)
{
    if (_linear < 0.0f)
    {
        _linear = 0.0f;
    }
    else if (_linear > 1.0f)
    {
        _linear = 1.0f;
    }

    return (_linear <= 0.0031308f ? _linear * 12.92f : 1.055f * Pow(_linear, 1.0f / 2.4f) - 0.055f);
}

inline f32 ExactSRGBToLinear(f32 _srgb)
{
    if (_srgb < 0.0f)
    {
        _srgb = 0.0f;
    }
    else if (_srgb > 1.0f)
    {
        _srgb = 1.0f;
    }

    return (_srgb <= 0.04045 ? _srgb / 12.92f : Pow((_srgb + 0.055f) / 1.055f, 2.4f));
}

inline v4 LinearToSRGB255(const v4 &_color)
{
    v4 result;

    result.r = 255.0f * ExactLinearToSRGB(_color.r);
    result.g = 255.0f * ExactLinearToSRGB(_color.g);
    result.b = 255.0f * ExactLinearToSRGB(_color.b);
    result.a = 255.0f * _color.a;

    return result;
}

inline v4 SRGB255ToLinear(const v4 &_color)
{
    v4 result;

    result.r = ExactSRGBToLinear(_color.r / 255.f);
    result.g = ExactSRGBToLinear(_color.g / 255.f);
    result.b = ExactSRGBToLinear(_color.b / 255.f);
    result.a = _color.a / 255.f;

    return result;
}

inline v4 LinearToSRGB(const v4 &_color)
{
    v4 result;

    result.r = ExactLinearToSRGB(_color.r);
    result.g = ExactLinearToSRGB(_color.g);
    result.b = ExactLinearToSRGB(_color.b);
    result.a = _color.a;

    return result;
}

inline v4 SRGBToLinear(const v4 &_color)
{
    v4 result;

    result.r = ExactSRGBToLinear(_color.r);
    result.g = ExactSRGBToLinear(_color.g);
    result.b = ExactSRGBToLinear(_color.b);
    result.a = _color.a;

    return result;
}

#endif // MATH_HH