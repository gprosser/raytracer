**A basic raytracer in C++**

* user-defined planes, spheres, materials
* saves to bitmap image format
* basic multithreading with std::thread
* object-oriented
* custom math library

![alt text](https://i.imgur.com/eYma6Gk.png "example output")