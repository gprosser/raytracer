#include "image.hh"
#include "defines.hh"
#include "math.hh"
#include <cstdio>
#include <cassert>
#include <cstring>

Image::Image(u32 _width, u32 _height)
{
    width = _width;
    height = _height;
    pixels = new v4[_width * _height];
}

Image::Image(const Image &_image)
{
    width = _image.width;
    height = _image.height;
    pixels = new v4[width * height];

    for (u32 y = 0; y < height; ++y)
    {
        for (u32 x = 0; x < width; ++x)
        {
            SetPixel(x, y, _image.GetPixel(x, y));
        }
    }
}

Image::~Image()
{
    delete[] pixels;
}

void Image::Resize(u32 _width, u32 _height)
{
    delete[] pixels;
    width = _width;
    height = _height;
    pixels = new v4[_width * _height];
}

void Image::Clear()
{
    if (pixels != nullptr)
    {
        std::memset(pixels, 0, width * height * sizeof(u32));
    }
}

v4 Image::GetPixel(u32 _x, u32 _y) const
{
    assert(_x < width && _y < height);
    return pixels[_y * width + _x];
}

void Image::SetPixel(u32 _x, u32 _y, const v4 &_color)
{
    assert(_x < width && _y < height);
    pixels[_y * width + _x] = _color;
}

b32 Image::LoadBMP(const char *_filename)
{
    FILE *fptr = fopen(_filename, "rb");
    if (fptr)
    {
        BMPHeader header = {};
        fread(&header, sizeof(header), 1, fptr);
        width = header.width;
        height = header.height;
        u32 *data = new u32[width * height];
        fseek(fptr, header.bitmapOffset, SEEK_SET);
        fread(data, header.sizeOfBitmap, 1, fptr);
        fclose(fptr);

        Resize(width, height);

        for (u32 y = 0; y < height; ++y)
        {
            for (u32 x = 0; x < width; ++x)
            {
                pixels[(height - y - 1) * width + x] = BGRAUnpack4x8(data[y * width + x]) / 255.f;
            }
        }

        delete[] data;

        return true;
    }
    else
    {
        return false;
    }
}

b32 Image::SaveBMP(const char *_filename)
{
    u32 *data = new u32[width * height];
    for (u32 y = 0; y < height; ++y)
    {
        for (u32 x = 0; x < width; ++x)
        {
            data[(height - y - 1) * width + x] = BGRAPack4x8(pixels[y * width + x] * 255.f);
        }
    }

    u32 outputSize = width * height * sizeof(u32);

    BMPHeader header = {};
    header.fileType = 0x4D42;
    header.fileSize = sizeof(header) + outputSize;
    header.bitmapOffset = sizeof(header);
    header.size = sizeof(header) - 14;
    header.width = width;
    header.height = height;
    header.planes = 1;
    header.bitsPerPixel = 32;
    header.compression = 0;
    header.sizeOfBitmap = outputSize;
    header.horzResolution = 0;
    header.vertResolution = 0;
    header.colorsUsed = 0;
    header.colorsImportant = 0;

    FILE *fptr = fopen(_filename, "wb");
    if (fptr)
    {
        fwrite(&header, sizeof(header), 1, fptr);
        fwrite(data, outputSize, 1, fptr);
        fclose(fptr);
        return true;
    }
    else
    {
        return false;
    }
}