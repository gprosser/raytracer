#include "scene.hh"
#include "defines.hh"
#include "math.hh"
#include "object.hh"
#include "image.hh"
#include <vector>
#include <thread>

Scene::Scene()
{
    background = {};
}

Scene::~Scene()
{
    for (Object *object : objects)
    {
        delete object;
    }
}

void Scene::Render(const Camera& _camera, Image &_image,
                   const u32 _raysPerPixel, const u32 _maxBounceCount) const
{
    const u32 imageWidth = _image.GetWidth();
    const u32 imageHeight = _image.GetHeight();

    std::thread threads[4];

    threads[0] = std::thread(&Scene::RenderTile, this, _camera, &_image, _raysPerPixel, _maxBounceCount, 0, imageWidth/2, 0, imageHeight/2);
    threads[1] = std::thread(&Scene::RenderTile, this, _camera, &_image, _raysPerPixel, _maxBounceCount, imageWidth/2, imageWidth, 0, imageHeight/2);
    threads[2] = std::thread(&Scene::RenderTile, this, _camera, &_image, _raysPerPixel, _maxBounceCount, 0, imageWidth/2, imageHeight/2, imageHeight);
    threads[3] = std::thread(&Scene::RenderTile, this, _camera, &_image, _raysPerPixel, _maxBounceCount, imageWidth/2, imageWidth, imageHeight/2, imageHeight);

    threads[0].join();
    threads[1].join();
    threads[2].join();
    threads[3].join();
}

void Scene::RenderTile(const Camera &_camera, Image *_image,
                       const u32 _raysPerPixel, const u32 _maxBounceCount,
                       const u32 _xMin, const u32 _xMax, const u32 _yMin, const u32 _yMax) const
{
    RandomSeries series {_xMax * 1234 + _yMax * 5678};

    f32 screenW = _camera.screenW;
    f32 screenH = _camera.screenH;

    const u32 imageWidth = _image->GetWidth();
    const u32 imageHeight = _image->GetHeight();

    if (imageWidth > imageHeight)
    {
        screenH = screenW * ((f32)imageHeight / (f32)imageWidth);
    }
    else if (imageHeight > imageWidth)
    {
        screenW = screenH * ((f32)imageWidth / (f32)imageHeight);
    }

    f32 halfScreenW = 0.5f * screenW;
    f32 halfScreenH = 0.5f * screenH;
    v3 screenCenter = _camera.P - _camera.screenDist * _camera.Z;

    f32 halfPixW = 0.5f / imageWidth;
    f32 halfPixH = 0.5f / imageHeight;

    const f32 rayContrib = 1.0f / (f32)_raysPerPixel;

    for (u32 y = _yMin; y < _yMax; ++y)
    {
        f32 screenY = 1.0f - 2.0f * ((f32)y / (f32)imageHeight);

        for (u32 x = _xMin; x < _xMax; ++x)
        {
            f32 screenX = -1.0f + 2.0f * ((f32)x / (f32)imageWidth);

            v3 color = {};

            for (u32 i = 0; i < _raysPerPixel; ++i)
            {
                f32 offX = screenX + RandomBilateral(&series) * halfPixW;
                f32 offY = screenY + RandomBilateral(&series) * halfPixH;
                v3 screenP = screenCenter + (offX * halfScreenW * _camera.X) + (offY * halfScreenH * _camera.Y);

                v3 rayOrigin = _camera.P;
                v3 rayDirection = NOZ(screenP - _camera.P);

                v3 sample = {};
                v3 attenuation = v3(1, 1, 1);

                for (u32 bounceCount = 0; bounceCount < _maxBounceCount; ++bounceCount)
                {
                    f32 hitDistance = F32_MAX;
                    Object *hitObject = nullptr;

                    for (Object *object : objects)
                    {
                        f32 t;
                        if (object->ComputeRayIntersection(rayOrigin, rayDirection, t) && t < hitDistance)
                        {
                            hitDistance = t;
                            hitObject = object;
                        }
                    }

                    if (hitObject != nullptr)
                    {
                        rayOrigin += hitDistance * rayDirection;
                        v3 nextNormal = hitObject->ComputeNormal(rayOrigin);

                        Material material = hitObject->material;

                        sample += Hadamard(attenuation, material.emitColor);
                        f32 cosAtten = Inner(-rayDirection, nextNormal);
                        if (cosAtten < 0)
                        {
                            cosAtten = 0;
                        }

                        attenuation = Hadamard(attenuation, cosAtten * material.reflectColor);

                        v3 pureBounce = rayDirection - 2.0f * Inner(rayDirection, nextNormal) * nextNormal;
                        v3 randomBounce = NOZ(nextNormal + v3(RandomBilateral(&series), RandomBilateral(&series), RandomBilateral(&series)));
                        rayDirection = NOZ(Lerp(randomBounce, material.scatter, pureBounce));
                    }
                    else
                    {
                        sample += Hadamard(attenuation, background.emitColor);
                        break;
                    }
                }

                color += rayContrib * sample;
            }

            _image->SetPixel(x, y, LinearToSRGB(v4(color, 1)));
        }
    }
}