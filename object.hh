#ifndef OBJECT_HH
#define OBJECT_HH

#include "defines.hh"
#include "math.hh"

struct Material
{
    v3 emitColor;
    v3 reflectColor;
    f32 scatter;
};

class Object
{
public:
    Material material;
    virtual b32 ComputeRayIntersection(const v3 &_origin, const v3 &_direction, f32 &_t) const { return false; }
    virtual v3 ComputeNormal(const v3 &_p) const { return v3(); }
};

class Plane : public Object
{
public:
    Plane(v3 _N, f32 _d, Material _material);
    v3 N;
    f32 d;
    virtual b32 ComputeRayIntersection(const v3 &_origin, const v3 &_direction, f32 &_t) const override;
    virtual v3 ComputeNormal(const v3 &_p) const override;
};

class Sphere : public Object
{
public:
    Sphere(v3 _P, f32 _r, Material _material);
    v3 P;
    f32 r;
    virtual b32 ComputeRayIntersection(const v3 &_origin, const v3 &_direction, f32 &_t) const override;
    virtual v3 ComputeNormal(const v3 &_p) const override;
};

#endif // OBJECT_HH