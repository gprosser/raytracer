#include "defines.hh"
#include "math.hh"
#include "image.hh"
#include "scene.hh"
#include "object.hh"
#include <ctime>
#include <cstdio>

int main(int argc, char **argv)
{
    // set up materials
    Material materials[7] = {};
    materials[0].emitColor = v3(0.3f, 0.4f, 0.5f);
    materials[1].reflectColor = v3(0.5f, 0.5f, 0.5f);
    materials[2].reflectColor = v3(0.7f, 0.5f, 0.3f);
    materials[3].emitColor = v3(4.0f, 0.0f, 0.0f);
    materials[4].reflectColor = v3(0.95f, 0.95f, 0.95f);
    materials[4].scatter = 0.99f;
    materials[5].reflectColor = v3(0.6f, 0.0f, 0.4f);
    materials[6].reflectColor = v3(0.2f, 0.2f, 0.4f);
    materials[6].scatter = 0.9f;

    // set up scene
    Scene scene;
    scene.SetBackground(materials[0]);
    scene.Add(new Plane(v3(0, 0, 1), 0, materials[1]));
    scene.Add(new Sphere(v3(0, 0, 0), 1, materials[2]));
    scene.Add(new Sphere(v3(3, -2, 1), 1, materials[3]));
    scene.Add(new Sphere(v3(-2, -1, 2), 1, materials[4]));
    scene.Add(new Sphere(v3(-3, -1.5, 1), 0.3f, materials[5]));
    scene.Add(new Sphere(v3(1, 5, 2), 2, materials[6]));

    // set up camera
    Camera camera;
    camera.P = v3(0, -10, 1);
    camera.Z = NOZ(camera.P);
    camera.X = NOZ(Cross(v3(0, 0, 1), camera.Z));
    camera.Y = NOZ(Cross(camera.Z, camera.X));

    // set up image
    Image image(1280, 720);

    // render scene
    timespec start, end;
    f32 elapsedMS;
    clock_gettime(CLOCK_MONOTONIC, &start);

    scene.Render(camera, image, 256);

    clock_gettime(CLOCK_MONOTONIC, &end);
    elapsedMS = 1000.0f * (end.tv_sec - start.tv_sec);
    elapsedMS += (end.tv_nsec - start.tv_nsec) / 1000000.0f;
    printf("completed in %0.fms\n", elapsedMS);

    // save image
    const char *filename = "output.bmp";
    image.SaveBMP(filename);
    printf("saved to %s\n", filename);

    return 0;
}