#include "object.hh"
#include "defines.hh"
#include "math.hh"

Plane::Plane(v3 _N, f32 _d, Material _material)
{
    N = _N;
    d = _d;
    this->material = _material;
}

b32 Plane::ComputeRayIntersection(const v3 &_origin, const v3 &_direction, f32 &_t) const
{
    const f32 tolerance = 0.0001f;
    const f32 minHitDistance = 0.001f;

    f32 denom = Inner(N, _direction);
    if (Abs(denom) > tolerance)
    {
        f32 t = (-d - Inner(N, _origin)) / denom;
        if (t > minHitDistance)
        {
            _t = t;
            return true;
        }
    }

    return false;
}

v3 Plane::ComputeNormal(const v3 &_p) const
{
    return N;
}

Sphere::Sphere(v3 _P, f32 _r, Material _material)
{
    P = _P;
    r = _r;
    this->material = _material;
}

b32 Sphere::ComputeRayIntersection(const v3 &_origin, const v3 &_direction, f32 &_t) const
{
    const f32 tolerance = 0.0001f;
    const f32 minHitDistance = 0.001f;

    v3 relativeOrigin = _origin - P;
    f32 a = Inner(_direction, _direction);
    f32 b = 2.0f * Inner(_direction, relativeOrigin);
    f32 c = Inner(relativeOrigin, relativeOrigin) - Square(r);

    f32 denom = 2.0f * a;
    f32 rootTerm = SquareRoot(b * b - 4.0f * a * c);
    if (rootTerm > tolerance)
    {
        f32 tp = (-b + rootTerm) / denom;
        f32 tn = (-b - rootTerm) / denom;

        f32 t = tp;
        if ((tn > minHitDistance) && (tn < tp))
        {
            t = tn;
        }

        if (t > minHitDistance)
        {
            _t = t;
            return true;
        }
    }

    return false;
}

v3 Sphere::ComputeNormal(const v3 &_p) const
{
    return NOZ(_p - P);
}