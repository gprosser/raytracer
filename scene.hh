#ifndef SCENE_HH
#define SCENE_HH

#include "defines.hh"
#include "math.hh"
#include "object.hh"
#include "image.hh"
#include <vector>

struct Camera
{
    v3 P;
    v3 X, Y, Z;
    f32 screenDist = 1.0f;
    f32 screenW = 1.0f;
    f32 screenH = 1.0f;
};

class Scene
{
public:
    Scene();
    ~Scene();

    inline void Add(Object *_object) { objects.push_back(_object); }
    inline void SetBackground(Material _material) { background = _material; }

    void Render(const Camera &_camera, Image &_image,
                const u32 _raysPerPixel = 128, const u32 _maxBounceCount = 8) const;

private:
    void RenderTile(const Camera &_camera, Image *_image,
                    const u32 _raysPerPixel, const u32 _maxBounceCount,
                    const u32 xMin, const u32 xMax, const u32 yMin, const u32 yMax) const;

    std::vector<Object *> objects;
    Material background;
};

#endif // SCENE_HH