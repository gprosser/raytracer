#ifndef IMAGE_HH
#define IMAGE_HH

#include "defines.hh"
#include "math.hh"

class Image
{
public:
    Image(u32 _width = 0, u32 _height = 0);
    Image(const Image &_image);
    ~Image();
    void Resize(u32 _width, u32 _height);
    void Clear();
    inline u32 GetWidth() const { return width; }
    inline u32 GetHeight() const { return height; }
    v4 GetPixel(u32 _x, u32 _y) const;
    void SetPixel(u32 _x, u32 _y, const v4 &_color);
    b32 LoadBMP(const char *_filename);
    b32 SaveBMP(const char *_filename);

private:
    u32 width;
    u32 height;
    v4 *pixels;

#pragma pack(push, 1)
    struct BMPHeader
    {
        u16 fileType;
        u32 fileSize;
        u16 reserved1;
        u16 reserved2;
        u32 bitmapOffset;
        u32 size;
        s32 width;
        s32 height;
        u16 planes;
        u16 bitsPerPixel;
        u32 compression;
        u32 sizeOfBitmap;
        s32 horzResolution;
        s32 vertResolution;
        u32 colorsUsed;
        u32 colorsImportant;
    };
#pragma pack(pop)
};

#endif // IMAGE_HH