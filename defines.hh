#ifndef DEFINES_HH
#define DEFINES_HH

#include <cstdint>
#include <cfloat>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;

typedef s32 b32;

typedef float f32;
typedef double f64;

#define F32_MAX FLT_MAX
#define U32_MAX ((u32)-1)

#endif // DEFINES_HH